﻿namespace DumpIndex
{
  using System;
  using System.Collections.Generic;
  using System.IO;
  using System.Linq;
  using System.Text;
  using Lucene.Net.Documents;
  using Lucene.Net.Search;
  using Lucene.Net.Store;
  using DumpIndex.Cmdline;
  using DumpIndex.Serialization;

  public static class Program
  {
    public static void Main(string[] args)
    {
      var cmdlineOptions = new Options();
      if (CommandLine.Parser.Default.ParseArguments(args, cmdlineOptions))
      {
        DoDumpIndex(cmdlineOptions);
      } else {
        Console.WriteLine(cmdlineOptions.GetUsage());
      }
      /*if (args.Length != 1)
      {
        Console.WriteLine("Wrong command-line parameters. Must be single parameter which points to folder with index.");
        return;
      }*/

    }

    private static void DoDumpIndex(Options cmdlineOptions)
    {
      var indexSearcher = new IndexSearcher(new SimpleFSDirectory(new DirectoryInfo(cmdlineOptions.InputIndexPath)));

      ISerializationStrategy serializationStrategy = null;
      switch (cmdlineOptions.OutputMode)
      {
        case OutputMode.Xml:
            serializationStrategy = new XmlSerializationStrategy();
            break;
        case OutputMode.FileSystem:
            serializationStrategy = new FsSerializationStrategy();
            break;
      }

      if (serializationStrategy == null)
      {
        throw new Exception("Failed to instantiate serialization strategy");
      }

      serializationStrategy.TrySerialize(indexSearcher, cmdlineOptions);
    }


    /// <summary>
    /// Gets sitecore://web/{ID}
    /// </summary>
    /// <param name="oneUri"></param>
    /// <returns></returns>
    private static string GetKey(string oneUri)
    {
      var key = oneUri.Substring("sitecore://".Length);
      key = key.Substring(key.IndexOf('/') + 1);
      key = key.Substring(0, key.IndexOf('}'));
      key = key.Replace("{", "").Replace("}", "").Replace("/", "_").Replace("-", "");
      return key;
    }

  }
}
