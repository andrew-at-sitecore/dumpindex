﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DumpIndex.Cmdline;
using Lucene.Net.Search;
using DumpIndex.Extensions;

namespace DumpIndex.Serialization
{
  class FsSerializationStrategy : ISerializationStrategy
  {
    public void TrySerialize(IndexSearcher indexSearcher, Options cmdlineOptions)
    {
      var oneCount = indexSearcher.MaxDoc;
      var groups = ListExtensions.For(0, oneCount).Select(i => ListExtensions.IndicateProgress(indexSearcher.Doc(i), i, 10000, oneCount, " READING "))
        .GroupBy(x => ListExtensions.GetIdLangVer(x.GetField("_uniqueid").StringValue))
        .GroupBy(x => ListExtensions.GetId(x.Key));

      var indexFullPath = Path.GetFullPath(cmdlineOptions.InputIndexPath).TrimEnd(Path.DirectorySeparatorChar);
      var targetFolder = Path.GetFileName(indexFullPath);

      Console.WriteLine(String.Format(">> Creating output folder: '{0}'", Path.GetFullPath(targetFolder)));

      if (Directory.Exists(targetFolder))
      {
        throw new Exception(String.Format("FS Lucene serializer - target folder '{0}' already exists. ( FullPath: '{1}' )", targetFolder, Path.GetFullPath(targetFolder)));
      }
      var sznRootDir = Directory.CreateDirectory(targetFolder);

      // iterating over "all items ( with nested versions )" group
      foreach (var group in groups)
      {
        var id = @group.Key;
        var contextDir = sznRootDir.CreateSubdirectory(id);
        // iterating over "all versions for the item" collection
        foreach (var documents in group)
        {
          var idLangVer = documents.Key;
          var langVer = idLangVer.Substring(idLangVer.IndexOf("lang=") + "lang=".Length);
          var pos = langVer.IndexOf("&ver=");
          var lang = langVer.Substring(0, pos);
          var ver = langVer.Substring(pos + "&ver=".Length);
          langVer = lang + "-" + ver;
          
          // Dumping content of an index document for particular language / version of particular Sitecore item
          using (var writer = File.CreateText(Path.Combine(contextDir.FullName, langVer)))
          {
            foreach (var document in documents)
            {
              foreach (var field in document.GetFields())
              {
                writer.WriteLine(String.Format("{0} : {1}", field.Name, field.StringValue));
              }
            }
          }

        }
      }

    }
  }
}

