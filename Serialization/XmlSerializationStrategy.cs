﻿using System;
using System.IO;
using System.Xml;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lucene.Net.Search;
using DumpIndex.Extensions;
using DumpIndex.Cmdline;

namespace DumpIndex.Serialization
{
  class XmlSerializationStrategy : ISerializationStrategy
  {
    public void TrySerialize(IndexSearcher indexSearcher, Options cmdlineOptions)
    {
      var oneCount = indexSearcher.MaxDoc;
      var groups = ListExtensions.For(0, oneCount).Select(i => ListExtensions.IndicateProgress(indexSearcher.Doc(i), i, 10000, oneCount, " READING "))
        .GroupBy(x => ListExtensions.GetIdLangVer(x.GetField("_uniqueid").StringValue))
        .GroupBy(x => ListExtensions.GetId(x.Key));

      var name = Path.GetFileName(cmdlineOptions.InputIndexPath);
      var xw = new XmlTextWriter(name + ".xml", new UTF8Encoding())
      {
        Formatting = Formatting.Indented
      };

      xw.WriteStartElement(name);
      foreach (var group in groups)
      {
        var id = @group.Key;
        xw.WriteStartElement(id);
        foreach (var documents in group)
        {
          var idLangVer = documents.Key;
          var langVer = idLangVer.Substring(idLangVer.IndexOf("lang=") + "lang=".Length);
          var pos = langVer.IndexOf("&ver=");
          var lang = langVer.Substring(0, pos);
          var ver = langVer.Substring(pos + "&ver=".Length);
          langVer = lang + "-" + ver;
          xw.WriteStartElement(langVer);
          foreach (var document in documents)
          {
            foreach (var field in document.GetFields())
            {
              xw.WriteElementString(field.Name, "", field.StringValue);
            }
          }
          xw.WriteEndElement();
        }
        xw.WriteEndElement();
      }

    }
  }

}
