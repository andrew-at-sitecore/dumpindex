﻿using DumpIndex.Cmdline;
using Lucene.Net.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DumpIndex.Serialization
{
  interface ISerializationStrategy
  {
    void TrySerialize(IndexSearcher indexSearcher, Options cmdlineOptions);


  }
}
