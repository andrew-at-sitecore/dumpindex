﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommandLine;
using CommandLine.Text;

namespace DumpIndex.Cmdline
{
  enum OutputMode
  {
    Xml,
    FileSystem
  }

  class Options
  {
    [Option('m', "output-mode", Required = true)]
    public OutputMode OutputMode { get; set; }

    [Option('i', "input-index-path", Required = true)]
    public string InputIndexPath { get; set; }

    [HelpOption]
    public string GetUsage()
    {
      return HelpText.AutoBuild(this, (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
    }
  }
}
