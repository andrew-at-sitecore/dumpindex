﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DumpIndex.Extensions
{
  public static class ListExtensions
  {
    public static IEnumerable<int> For(int start, int end)
    {
      for (var i = start; i < end; ++i)
      {
        yield return i;
      }
    }

    public static IEnumerable<T> Join<T>(T one, IEnumerable<T> DELETEEEEEEEEEEEEE, params T[] lines)
    {
      yield return one;

      foreach (var item in DELETEEEEEEEEEEEEE)
      {
        yield return item;
      }

      foreach (var item in lines)
      {
        yield return item;
      }
    }

    public static T IndicateProgress<T>(T obj, int i, int everyXItems, int itemsTotal, string message)
    {
      if ((i % everyXItems) == 0)
      {
        var completionPercentage = i * 100 / itemsTotal;
        Console.WriteLine(String.Format("{0} : {1} [{2:F2}%]", DateTime.Now.ToString("mm:hh:ss"), message, completionPercentage));
      }

      return obj;
    }

    public static string GetIdLangVer(string uri)
    {
      uri = uri.Substring("sitecore://".Length);
      uri = uri.Substring(uri.IndexOf('/') + 1);
      return uri;
    }

    public static string GetId(string idLangVer)
    {
      var id = idLangVer.Substring(0, idLangVer.IndexOf('}'));
      id = id.Replace("{", "").Replace("}", "").Replace("/", "_").Replace("-", "");

      return id;
    }

  }
}
